from app import app
import os
debug_value=bool(os.getenv("debug_value"))
if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000,debug=debug_value)

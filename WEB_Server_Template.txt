#!/bin/bash

sudo su
yum update -y && yum install git -y && amazon-linux-extras install nginx1 -y

mkdir -p /tmp/web_dir && git clone https://gitlab.com/mech.teacher20/cistek_3tier.git /tmp/web_dir && unzip /tmp/web_dir/flask_app.zip -d /tmp/web_dir/ && cd /tmp/web_dir/flask_app


mv /etc/nginx/nginx.conf /etc/nginx/nginx1.conf && mv -f nginx.conf  /etc/nginx/


#<=======PASTE APP ALB dns From AWS=====>
cat > /etc/environment <<EOF
export appALBdns=172.0.11.9
EOF


mkdir -p /etc/nginx/sites-enabled && source /etc/environment && mv flask_app /etc/nginx/sites-enabled/ && sed -i "s/APPalbDNS/$appALBdns/g" /etc/nginx/sites-enabled/flask_app

mv -f /usr/share/nginx/html/index.html /usr/share/nginx/html/index1.html && mv -f templates/smash-free-lite/* /usr/share/nginx/html/


nginx -t
systemctl start nginx
nginx -s reload

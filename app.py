from flask import Flask,render_template,request
import pymysql
import os
import socket

app = Flask(__name__)
print(app.static_folder,app.static_url_path,app.template_folder)

db_host=os.getenv("db_host")
db_user=os.getenv("db_user")
db_pswd=os.getenv("db_pswd")
db_name=os.getenv("db_name")
db_port=int(os.getenv("db_port"))
host_ip=os.getenv("host_ip")
host_az=os.getenv("host_az")

def conn():
    conn = pymysql.connect(host=db_host, user=db_user, password=db_pswd, db=db_name, port=db_port)
    channel = conn.cursor()
    return conn, channel
#def conn():
#    conn = pymysql.connect(host='localhost', user='root', password='Kunal_root1', db='flask_db', port=3306)
#    channel = conn.cursor()
#    return conn, channel


def acc_balance(con='', chanel=''):
    if con and chanel:
        quer = f"SELECT acc_bal from flask_table WHERE acc_id = 1"
        chanel.execute(quer)
        quer_data = chanel.fetchone()
        acc_bal_db = quer_data[0]
        return acc_bal_db
    else:
        conn_channel = conn()
        con = conn_channel[0]
        chanel = conn_channel[1]
        quer = f"SELECT acc_bal from flask_table WHERE acc_id = 1"
        chanel.execute(quer)
        quer_data = chanel.fetchone()
        acc_bal_db = quer_data[0]
        con.close()
        return acc_bal_db


def acc_update(acc_bal_db):
    conn_channel = conn()
    con = conn_channel[0]
    chanel = conn_channel[1]
    querr = f"UPDATE flask_table SET acc_bal={acc_bal_db} WHERE acc_id=1"
    chanel.execute(querr)
    con.commit()
    acc_bal_db = acc_balance(con, chanel)
    con.close()
    return acc_bal_db

@app.route('/',methods = ["GET"])
def ser_ip():
    # ip = (socket.gethostname())
    # ser_ip = ip.split(".")[0]
    # print ("HI on the console Server 2")
    # print(os.getpid())
    #return render_template('server_name.html', host_ip='localhost', host_az='local')
    return render_template('server_name.html', host_ip=host_ip,host_az=host_az)
    #return f"This CISTEK DEMO WEB APP is hosted on {host_ip}, in the AWS availability zone : {host_az}...! "

@app.route('/acc/',methods = ["GET"])
def account_balance():
    if request.method == "GET":
        acc_bal_db = acc_balance()
        msg_flask = f"""
                    Your Account balance is {acc_bal_db}
                    """
        return render_template('acc_page.html',msg_html= msg_flask)


@app.route('/transaction/', methods=["POST"])
def transaction():
    if request.method == "POST":
        formdata = request.form
        acc_bal_db = acc_balance()
        if formdata.get('option') == 'amount_add':
            amount_add_flask = int(formdata.get('amount'))
            acc_bal_db = acc_bal_db+amount_add_flask
            acc_update(acc_bal_db)
            acc_bal_db = acc_balance()
            msg_flask = f"Amount added successfully..!!   Your Current Account balance is {acc_bal_db} "
            return render_template('acc_page.html', msg_html= msg_flask)

        if formdata.get('option') == 'amount_deduct':
            amount_deduct_flask = int(formdata.get('amount'))
            acc_bal_db = acc_bal_db-amount_deduct_flask
            acc_update(acc_bal_db)
            acc_bal_db = acc_balance()
            msg_flask = f"Amount Deducted successfully..!!   Your Current Account balance is {acc_bal_db} "
            return render_template('acc_page.html', msg_html= msg_flask)



# if __name__ == '__main__':
#     app.run(debug=True)
